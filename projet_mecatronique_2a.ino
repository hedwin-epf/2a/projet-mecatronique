#include <Servo.h>

//CLASS RELATIVE A LA SUIVIE DE LIGNE
/*class Ligne{
private:
    int _pinG;
    int _pinM;
    int _pinD;
public:
    void init(int pinD, int pinM, int pinG);  // initialisation de la broche
    int result();
};
void Ligne::init(int pinD, int pinM, int pinG){
    pinMode(pinD, INPUT);
    pinMode(pinM, INPUT);
    pinMode(pinG, INPUT);
    _pinD = pinD; _pinM = pinM; _pinG = pinG;
}
//gauche : 0, droite: 2, tout droit : 1, reculer : -1
int Ligne::result(){
    bool eG = digitalRead(_pinG);
    bool eD = digitalRead(_pinD);
    bool eM = digitalRead(_pinM);
    if(eM){
        if(!eG && eD) return 2;
        else if(eG && !eD) return 0;
        else return 1;
    }else{
        if(!eG && eD) return 2;
        else if(eG && !eD) return 0;
        else return -1;
    }
}*/

//CLASS RELATIVE A LA DIRECTION
class Direction{
private:
    Servo servo;
public:
    void init(int attachPinServo);  // initialisation de la broche
    void right();
    void left();
    void straight();
    void specifyAngle(float angle);
};
void Direction::init(int attachPinServo){
    servo.attach(attachPinServo);
}
void Direction::right(){
    servo.write(45);
}
void Direction::left(){
    servo.write(135);
}
void Direction::straight(){
    servo.write(90);
}
void Direction::specifyAngle(float angle){
    servo.write(angle);
}

//CLASS RELATIVE AU DRAPEAU
class Drapeau{
private:
    Servo servo;
public:
    void init(int attachPinServo);  // initialisation de la broche
    void up();
    void down();
};
void Drapeau::init(int attachPinServo){
    servo.attach(attachPinServo);
}
void Drapeau::up(){
    servo.write(180);
}
void Drapeau::down(){
    servo.write(0);
}

//CLASS RELATIVE AU CAPTEUR ULTRASON
class CapUltrason{
private:
    int t_pin;
    int p_pin;
public:
    void init(int trig_pin, int pulse_pin);  // initialisation de la broche
    long getDuration();
    long getDistanceCentimeter();
};
void CapUltrason::init(int trig_pin, int pulse_pin){
    pinMode(trig_pin, OUTPUT);
    pinMode(pulse_pin, INPUT);
    t_pin = trig_pin; p_pin = pulse_pin;
}
long CapUltrason::getDuration(){
    digitalWrite(t_pin, 0);
    digitalWrite(t_pin, 1);
    digitalWrite(t_pin, 0);
    return pulseIn(p_pin, 1);
}
long CapUltrason::getDistanceCentimeter(){
    return (340 * getDuration() * 0.5 * 0.000001) * 100;
}

//-----------------------------------------------------------
Direction direction;
Drapeau drap;
//Ligne ligne;
CapUltrason u1;
CapUltrason u2;

long distance;


void setup(){
    Serial.begin(9600);
    u1.init(3,4);
    u2.init(6,7);
    direction.init(2);
    drap.init(12);
    //ligne.init(9,10,11);
}

void loop(){
    long d1 = u1.getDistanceCentimeter();
    Serial.println("Capteur 1 - "+String(d1)+"cm");
    long d2 = u2.getDistanceCentimeter();
    Serial.println("Capteur 2 - "+String(d2)+"cm");

    //fraction des distances
    double f = (double)d1/(double)d2;
    //multiplicateur (plage: -3 à 3)
    float a = log(f);
    //angle 90 (position 0) plage: 30° à 150°
    int angle = 90+a*20;

    Serial.print("Angle: "+String(angle));
    direction.specifyAngle(angle);

    drap.up();

    delay(1000);
}